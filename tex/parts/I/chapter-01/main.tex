\chapter{Essence Info}
The project you will be developing in this chapter is an \verb|Essence Info| Cloud UX application which is similar to the standard \verb|File Info| but is much simpler.


\section{Create an empty project}
There is nothing at the moment so let's create something.


\subsection{Directories and git repository}
The first step is to configure the project directory and initialize a git repository.

\begin{minted}{bash}
  $ mkdir -p essence-info/{src,docker,scripts}
  $ cd essence-info
  $ git init
  $ echo "/yarn-error.log\n/build\n/node_modules" > .gitignore
  $ echo "Copyright (C) 2020 by Avid Technology, Inc." > LICENSE.md
\end{minted}

\begin{wraptable}{r}{7cm}
  \centering
  \begin{tabular}{ l l l }
    \textbf{Directory} & \textbf{Description} \\
    \verb|src|         & Application sources  \\
    \verb|docker|      & Docker configuration \\
    \verb|scripts|     & Build scripts
  \end{tabular}
  \caption{Standard project directories}\label{tab:essence-info-standard-project-dirs}
\end{wraptable}

One of the commands created an initial directory structure for the project which consists of the absolute minimum number of directories you need to properly organise it; you will add more directories later, once you start adding more and more features to the application, use more tools, and more complex configurations.

The next step is to check if \verb|.gitignore| is correct:

\begin{minted}{bash}
  $ cat .gitignore
  /yarn-error.log
  /build
  /node_modules
\end{minted}

\subsection{Package metadata}

Create a new \verb|package.json| and add standard metadata such as project name, version, author, and more:

\begin{minted}{json}
  {
    "name": "avid-mam-ps-clux-essence-info",
    "version": "0.0.1",
    "description": "Cloud UX plug-in tutorial",
    "author": "Oleksandr Manenko <oleksandr.manenko@avid.com>",
    "license": "SEE LICENSE IN LICENSE.md",
    "maintainers": [
      {
        "name": "Oleksandr Manenko",
        "email": "oleksandr.manenko@avid.com"
      }
    ]
  }
\end{minted}

\noindent
Run \verb|yarn| to check if everything is correct:

\begin{minted}{bash}
  $ yarn
  info No lockfile found.
  [1/4] Resolving packages...
  [2/4] Fetching packages...
  [3/4] Linking dependencies...
  [4/4] Building fresh packages...

  success Saved lockfile.
  Done in 0.05s.
\end{minted}

\noindent
Commit your changes:

\begin{minted}{bash}
  $ git add .
  $ git commit -m "Create an empty project"
  [master (root-commit) 120d34c] Create an empty project
  4 files changed, 21 insertions(+)
  create mode 100644 .gitignore
  create mode 100644 LICENSE.md
  create mode 100644 package.json
  create mode 100644 yarn.lock
\end{minted}

\section{Development dependencies}

There are a few tools you need to develop Cloud UX plug-ins.
Some of these tools are developed internally and you have to configure your Node package manager before installing them.

Create a new \verb|.npmrc| file and add the following lines that configure package manager to search for product (\verb|@avid-technology|) and professional services (\verb|@avid-ps|) scoped\footnote{You can learn more about scoped packages at \url{https://docs.npmjs.com/about-scopes}} packages in the internal repositories:

\begin{minted}[breakbefore=/]{text}
  @avid-technology:registry=http://gl-nexus.global.avidww.com:8081/content/groups/npm-all/
  @avid-ps:registry=http://gl-nexus.global.avidww.com:8081/content/groups/mam.ps.npm/
  timeout=360000
\end{minted}

If later you ask \verb|yarn| to install a package, which scope is \verb|@avid-ps|, it will look for the package in the correct registry\footnote{https://gl-nexus.global.avidww.com:8081/\ldots/mam.ps.npm/}.

\subsection{Installation}

Install the following development dependencies:

\begin{minted}{bash}
  $ yarn add -D \
  @avid-ps/gulp-clux \
  @babel/cli \
  @babel/core \
  @babel/plugin-proposal-class-properties \
  @babel/plugin-proposal-throw-expressions \
  @babel/plugin-transform-runtime \
  @babel/preset-env \
  @babel/preset-react \
  babel-eslint \
  babel-loader \
  base64-img \
  brotli-webpack-plugin \
  compression-webpack-plugin \
  copy-webpack-plugin \
  css-loader \
  del \
  eslint \
  eslint-plugin-import \
  gulp \
  prettier \
  style-loader \
  svg-url-loader \
  url-loader \
  webpack \
  webpack-cli \
  webpack-merge
\end{minted}

\subsection{Configuration}

Modern JavaScript is well known for a lot of configuration needed to be done before starting writing any code.

\subsubsection{Configure Babel}

Babel is a toolchain that is mainly used to convert ECMAScript 2015+ code into a backwards compatible version of the worst\footnote{\url{https://hackernoon.com/the-javascript-phenomenon-is-a-mass-psychosis-57adebb09359}} language in the world in current and older browsers or environments.

Create the \verb|babel.config.json| file and enable class properties, throw expressions; enable transform runtime plug-in; enable react and env presets:

\begin{minted}{json}
  {
    "presets": ["@babel/preset-env", "@babel/preset-react"],
    "plugins": [
      "@babel/plugin-proposal-class-properties",
      "@babel/plugin-proposal-throw-expressions",
      "@babel/plugin-transform-runtime"
    ]
  }
\end{minted}

\begin{exercise}[Learning Babel]{exrcs:learning-babel}
  Refer to the Babel official site\footnote{\url{https://babeljs.io/docs/en/}} for an explanation of the plug-ins and presets used in this chapter.
  Answer the following questions:
  \begin{itemize}
  \item What is Babel preset?
  \item Name a few common presets and explain their purpose.
  \item What are \verb|Stage-X| presets?
  \item What is \verb|browserslist| and how do you integrate it with the \verb|@babel/preset-env|?
  \item What is the purpose of the \verb|@babel/plugin-transform-runtime| plug-in?
  \end{itemize}
\end{exercise}

\subsubsection{Configure ESLint}

ESLint\footnote{\url{https://eslint.org/}} statically analyzes your code to quickly find problems. Create the \verb|.eslintrc.js| file and add the following code:

\begin{minted}{javascript}
  module.exports = {
    env: {
      browser: true,
      es6: true,
      node: false,
    },
    extends: "eslint:recommended",
    globals: {
      Atomics: "readonly",
      SharedArrayBuffer: "readonly",
    },
    parser: "babel-eslint",
    parserOptions: {
      ecmaVersion: 2020,
      sourceType: "module",
    },
  };
\end{minted}

The configuration uses \verb|eslint:recommended| set of rules, sets ECMAScript version to the latest available, etc.

\subsubsection{Configure Prettier}

Prettier\footnote{\url{https://prettier.io/docs/en/index.html}} is a code formatter.
It removes all original styling and ensures that all outputted code conforms to a consistent style.
Create the \verb|.prettierrc.js| file and add the following code:

\begin{minted}{javascript}
  module.exports = {
    arrowParens: "always",
    printWidth: 120,
  };
\end{minted}


\subsubsection{Pause for a moment}

So at this point you have configured:

\begin{enumerate}
\item A tool that transforms a so called \textit{modern} JavaScript into an old version of the same language supported by many browsers (even so Cloud UX supports Google Chrome only). The tool is \verb|Babel|.
\item A tool  that checks your source  code for well-known issues. The tool is \verb|ESLint|.
\item A tool that reformats your code to conform the style it defines. The tool is \verb|Prettier|.
\end{enumerate}

You can invoke both ESLint and Prettier from the command-line but it is more convenient if your text editor has integration with these tools.

\begin{exercise}[Configure your editor to use ESLint and Prettier]{exrcs:configure-editor-for-eslint-prettier}
  Refer to the documentation of your editor/IDE and integrate it with ESLint and Prettier.
  
  The editor should run ESLint either via keyboard shortcut or automatically, while you are writing your code.
  There are errors that could be fixed automatically and ESLint can do that.
  The editor should provide a way to do that too.

  The same applies to Prettier.
  The best approach if the editor can use Prettier to reformat the code on the go.
\end{exercise}

There are a few things missing, though.
For example, you need something that can manage resources (style-sheets, json data files, icons), invoke the tools you configured previously, etc.
On the other words, you need something that can build your project.
The tool you need is \verb|Webpack|.

\subsubsection{Webpack configuration}

Make sure you have read the documentation\footnote{\url{https://webpack.js.org/concepts/}} and have at least some understanding of what is Webpack, what it is capable of, etc. Then create the \verb|webpack.common.config.js| file and add the following code:

\begin{minted}{javascript}
  const CopyPlugin = require("copy-webpack-plugin");
  const LoaderOptionsPlugin = require("webpack").LoaderOptionsPlugin;
  const fs = require("fs");
  const path = require("path");
  const base64Img = require("base64-img");

  const appDirectory = fs.realpathSync(process.cwd());
  const resolveAppPath = (relPath) => path.resolve(appDirectory, relPath);

  const pathConfig = {
    buildDir: resolveAppPath("build"),
    nodeModulesDir: resolveAppPath("node_modules"),
    appEntry: resolveAppPath("src/index.js"),
    appConfigFile: resolveAppPath("src/package.json"),
    appSrcDir: resolveAppPath("src"),
  };

  const icon = base64Img.base64Sync(path.resolve(pathConfig.appSrcDir, "images", "icon.svg"));
  const iconJson = path.resolve(pathConfig.appSrcDir, "images", "icon.json");
  fs.writeFileSync(iconJson, JSON.stringify({ icon: icon }));

  module.exports = {
    entry: pathConfig.appEntry,
    output: {
      path: pathConfig.buildDir,
      filename: "index.js",
      libraryTarget: "amd",
    },

    resolve: {
      modules: [pathConfig.nodeModulesDir],
      extensions: [".json", ".js", ".jsx"],
    },

    module: {
      rules: [
        {
          test: /\.jsx?$/,
          loader: "babel-loader",
          exclude: /node_modules/,
        },
        {
          test: /\.svg$/,
          use: "svg-url-loader",
        },
        {
          test: /\.css$/i,
          use: [
            {
              loader: "style-loader",
            },
            {
              loader: "css-loader",
              options: {
                modules: {
                  compileType: "module",
                  localIdentName: "[path][name]__[local]--[hash:base64:5]",
                },
              },
            },
          ],
        },
      ],
    },

    plugins: [
      new LoaderOptionsPlugin({
        minimize: true,
      }),
      new CopyPlugin({
        patterns: [
          {
            from: pathConfig.appConfigFile,
            to: ".",
            flatten: true,
          },
        ],
      }),
    ],
  };
\end{minted}

This is a lot of code!
On the other hand, most of it is a configuration of the packages you installed before.

First, I create the \verb|pathConfig| object which contains paths to important project directories:

\begin{minted}{javascript}
  const appDirectory = fs.realpathSync(process.cwd());
  const resolveAppPath = (relPath) => path.resolve(appDirectory, relPath);

  const pathConfig = {
    buildDir: resolveAppPath("build"),
    nodeModulesDir: resolveAppPath("node_modules"),
    appEntry: resolveAppPath("src/index.js"),
    appConfigFile: resolveAppPath("src/package.json"),
    appSrcDir: resolveAppPath("src"),
  };
\end{minted}

Then, I encode an application icon in SVG format using base64 encoding.
I do this each time during the build and save it inside the \verb|icon.json| file:

\begin{minted}{javascript}
  const icon = base64Img.base64Sync(path.resolve(pathConfig.appSrcDir, "images", "icon.svg"));
  const iconJson = path.resolve(pathConfig.appSrcDir, "images", "icon.json");
  fs.writeFileSync(iconJson, JSON.stringify({ icon: icon }));
\end{minted}

The rest of the code configures Webpack plug-ins and loaders.
Webpack will:
\begin{itemize}
\item use Babel to transform JavaScript files
\item load SVG files as base64 encoded data URL strings
\item inject CSS to the DOM
\item allow importing styles as modules in JavaScript
\item generate unique CSS class names under the hood
\end{itemize}

\begin{exercise}[Getting familiar with Webpack loaders]{exrcs:getting-familiar-with-webpack-loaders}
  Read about Webpack loaders\footnote{\url{https://webpack.js.org/loaders/}}.

  Pay attention to the following loaders in particular:
  \begin{itemize}
    \item \verb|babel-loader|
    \item \verb|svg-url-loader|
    \item \verb|style-loader|
    \item \verb|css-loader|
  \end{itemize}

  You should be able to explain their purpose, configuration, and usage.
\end{exercise}

Have you noticed that the script refers to a few files that do not exist yet?
You will add them in a few sections.

The next step is to add two more Webpack scripts: one for production builds and one for development builds.
Production build script will minimize and compress the code and resources to ensure the smallest bundle size possible.
Development build will add source maps\footnote{\url{https://developer.mozilla.org/en-US/docs/Tools/Debugger/How_to/Use_a_source_map}} to easier your debugging and troubleshooting experience while working on development and quality-assurance systems.

Create the \verb|webpack.prod.config.js| and add the following code:

\begin{minted}{javascript}
  const { merge } = require("webpack-merge");
  const CompressionPlugin = require("compression-webpack-plugin");
  const BrotliPlugin = require("brotli-webpack-plugin");
  const common = require("./webpack.common.config");

  module.exports = merge(common, {
    mode: "production",
    optimization: {
      minimize: true,
    },
    devtool: false,
    plugins: [
      new CompressionPlugin({
        filename: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.js$|\.css$/,
        threshold: 10240,
        minRatio: 0.8,
      }),
      new BrotliPlugin({
        asset: "[path].br[query]",
        test: /\.(js|css|svg)$/,
        threshold: 10240,
        minRatio: 0.8,
      }),
    ],
  });
\end{minted}

Here, I merge the configuration from the \verb|webpack.common.config.js| with the new one which disables development tools and compresses the bundle.

Finally, create the \verb|webpack.dev.config.js| which generates inline source maps:

\begin{minted}{javascript}
  const { merge } = require("webpack-merge");
  const common = require("./webpack.common.config");

  module.exports = merge(common, {
    mode: "development",
    devtool: "inline-source-map"
  });
\end{minted}

To run a development build invoke the \verb|webpack| command-line tool directly and provide it with the path to the script:

\begin{minted}[linenos=false]{javascript}
  $ npx webpack --config webpack.dev.config.js --display-error-details
\end{minted}

It will fail because there are a few files missing.
Run it anyway to become familiar with Webpack error messages.

The command you invoke to build the project is too long, that is why the next step is to add short aliases and hide long and hard to remember build commands. Open \verb|package.json| and add \verb|scripts| object to the root:

\begin{minted}{json}
  {
    "scripts": {
      "build:dev": "npx webpack --config webpack.dev.config.js --display-error-details",
      "build:prod": "npx webpack --config webpack.prod.config.js --display-error-details"
    }
  }
\end{minted}

Now you can build the project using either of:

\begin{minted}{bash}
  $ yarn run build:dev
  $ yarn run build:prod
\end{minted}

\subsection{Pause and reflect}
You configured Webpack for both production and development builds.
There are source files that do not exist yet, though.
You will add them in the next section.
Meanwhile, it is time to commit your changes:

\begin{minted}{bash}
  $ git add .
  $ git commit -m "Configure linter and build tools"
\end{minted}

\section{Add source code}

In this section we will add the following sources, assets, and configuration files to make the plug-in build successfully:

\begin{table}[ht]
  \centering
  \begin{tabular}{ l l l }
    \textbf{File}                  & \textbf{Description}  \\
    \verb|src/package.json|        & Avid plug-in metadata  \\
    \verb|src/project.config.json| & Avid project metadata \\
    \verb|src/images/icon.svg|     & Plug-in icon          \\
    \verb|src/index.js|            & Source code that exports a special Cloud UX object
  \end{tabular}
  \caption{Remanining files to add}\label{tab:essence-info-remaining-files}
\end{table}

\subsection{src/package.json}

This file is your product metadata.
Go to \url{https://my.avid.com/shop/businessorientation} and register a new application if you have not done it before.

Edit the file and add the following content to it (change the application data to the one you got from the registration page):

\begin{minted}{json}
  {
    "identity": {
      "appName": "avid-mam-ps-clux-essence-info"
    },
    "main": "index.js",
    "avid": {
      "format": "amd",
      "alias": "avid-mam-ps-clux-essence-info-y92mRHEetQpblwnJHwRROZKG8f69hqJZHf",
      "secret": "uPdMNfrImZjzg+oeKzLA0WlBmMLYvJ6elZOv6nElWGM=",
      "autoload": false,
      "features": {
        "provides": {
          "apps": [
            {
              "name": "avid-mam-ps-clux-essence-info",
              "config": {
                "id": "avid-mam-ps-clux-essence-info",
                "allowedContext": [
                  {
                    "mimeType": "text/x.avid.asset-list",
                    "systemType": "interplay-mam"
                  },
                  {
                    "mimeType": "text/x.avid.asset-list+json",
                    "systemType": "interplay-mam"
                  }
                ]
              }
            }
          ]
        }
      }
    }
  }
\end{minted}

The \verb|allowedContext|\footnote{\url{https://developer.avid.com/mcux_ui_plugin/clux-api/apps/allowed-context.html}} key describes what kind of objects the plug-in can open.
I will describe it in more details later.

\subsection{src/project.config.json}

Create the file and add the following content:

\begin{minted}{json}
  {
    "identity": {
      "description": "Cloud UX plug-in tutorial.",
      "build": "1",
      "version": "0.0.1"
    },
    "signing": {
      "login": "",
      "developerID": "",
      "organization": "avid",
      "privateKeyPath": ""
    },
    "connection": {
      "hostIp": "kl-psg-mcs89.global.avidww.com",
      "proxyPort": "8080"
    }
  }
\end{minted}

You have seen this file before.

\subsection{src/images/icon.svg}

This is an icon for the application.
Create the file with the following contents:

\begin{minted}{xml}
  <?xml version="1.0" encoding="utf-8"?>
  <svg xmlns="http://www.w3.org/2000/svg"
       width="24"
       height="24"
       viewBox="0 0 24 24"
       fill="none"
       stroke="currentColor"
       stroke-width="2"
       stroke-linecap="round"
       stroke-linejoin="round"
       class="feather feather-square">
    <rect x="3" y="3" width="18" height="18" rx="2" ry="2"/>
  </svg>
\end{minted}

This is a plain sad square.

\subsection{src/index.js}

This is a starting point of the application.
The script must export an object which \verb|avid| property is an array of the plug-in metadata:

\begin{minted}{javascript}
  {
    avid: [
      {
        name: ...,
        provides: ...,
        create: ...
      },
    ]
  }
\end{minted}

Create the file and add the following code:

\begin{minted}{javascript}
import appConfig from "./package.json";
import icon from "./images/icon.svg";

class ApplicationViewWrapper {
  static Id = `${appConfig.identity.appName}-view`;

  constructor() {
    this.rootElement = null;
  }

  onRender = ({ domElement }) => {
    this.rootElement = document.createElement("div");
    const content = document.createElement("span");
    content.innerText = "Hello, world!";
    this.rootElement.appendChild(content);

    domElement.appendChild(this.rootElement);
  };

  onDestroy = () => {
    // You will need this one later, once we start using React.
  };

  setContext = (context) => {
    console.log(`[${ApplicationViewWrapper.Id}] setContext`, context);
  };

  get publicScope() {
    return {
      setContext: this.setContext,
    };
  }
}

class Application {
  constructor() {
    this.view = null;
  }

  getLayoutConfig = () => {
    return {
      type: ApplicationViewWrapper.Id,
    };
  };

  onRender = (header, views) => {
    this.view = Object.assign({}, views[ApplicationViewWrapper.Id]);
  };

  setContext(context) {
    console.log(`[${appConfig.identity.appName}] setContext`, context);
    if (this.view) {
      this.view.setContext(context);
    }
  }

  get publicScope() {
    return {};
  }
}

const avid = [
  {
    name: appConfig.identity.appName,
    provides: ["apps"],
    create: () => {
      return {
        factory: () => new Application(),
        config: {
          title: "Essence Info",
          allowedContext: [
            {
              mimeType: "text/x.avid.asset-list",
              systemType: "interplay-mam",
            },
            {
              mimeType: "text/x.avid.asset-list+json",
              systemType: "interplay-mam",
            },
          ],
        },
        menuIcon: {
          group: 200,
          orderInGroup: 200,
          title: appConfig.identity.appName,
          icon: icon.icon,
          gradient: ["#ba2f82", "#cf4c85"],
        },
      };
    },
  },
  {
    name: ApplicationViewWrapper.Id,
    provides: ["appViews"],
    create: () => {
      return {
        factory: () => new ApplicationViewWrapper(),
        config: {},
      };
    },
  },
];

export { avid };
\end{minted}

There are a few important bits here.

\subsubsection{Special `avid' object}

I export \verb|avid| object which is an application configuration used by Cloud UX to understand what capabilities the plug-in has.
Here, I declare that plug-in provides an \verb|app| and \verb|appView|.
I instruct Cloud UX that the plug-in can open Asset Management assets, so Cloud UX must register it as an `Open in' application.

\subsubsection{Application class}

The \verb|Application| class defines is an entry-point of the UI plug-in and has a few important members:

\begin{description}
\item[publicScope]
  An object which members are accessible in menu icon handlers and actions plugged to this application.
\item[getLayoutConfig]
  A method is used by Cloud UX to understand what it should do to create a UI for the application.
  In this case, I simply tell it that there is one view which Cloud UX should create for me.
\item[onRender]
  A method called after rendering DOM with all views defined in the layout.
  The \verb|views| object consists of the views that belong to the plug-in.
  Object keys are equal to view type or label. And the objects are those returned by the view's \verb|publicScope| property.
  In other words, you do not have a direct access to a view, only to the object the view defined as its \verb|publicScope|.
  I copy the view and save it as a member of this class.
\item[setContext]
  A method called by Cloud UX when it wants to open an asset in this application.
  I use the previously saved view's \verb|publicScope| to tell it which asset should be opened.
\end{description}

That's it.
The application tells Cloud UX which layout it supports, listens to the context changes, and sends these changes to the view.

\subsubsection{ApplicationViewWrapper class}
This is what I call an \verb|appView| in the previous section.
This is what Cloud UX calls an application \textbf{view}.
The terminology, while being completely legal and logical, could be a bit misleading: this object is not attached to the DOM.
Instead, it attaches the real HTML elements during its lifecycle.
This is why I call the class a \textbf{view wrapper}.
Let's go through its members:

\begin{description}
\item[publicScope]
  An object that exposes a view public API.
  It will be accessible in an application that owns the view (\verb|Application| class), actions (view menu items), keyboard shortcuts handlers.
  There is only one member defined here: \verb|setContext|, which is a method I call from the \verb|Application| class, when Cloud UX wants to open an asset in this application.
\item[onRender]
  It is called when view DOM element is created.
  Be aware it might be that view is not in the DOM yet.
  This is where we attach the real HTML elements to the parent element provided by Cloud UX.
  I create a simple \verb|span| for now but in the future this is where \verb|ReactDOM.render| should be called.
\item[onDestroy]
  It is called before the view is destroyed. This is the place to cleanup resources, if any. For example, this is where \verb|ReactDOM.unmountComponentAtNode| should be called.
\end{description}

\subsection{Build the project and commit the changes to the repository.}

Run \verb|yarn run build:dev| and \verb|yarn run build:prod| to ensure everything builds without errors.
Check your build directory. Commit the changes to git.

\section{Package and deploy}

So you have build the plug-in but how do you deploy the application to the Cloud UX system?
You need a feature pack.

\subsection{Create a feature pack}

Cloud UX feature pack is a Helm\footnote{\url{https://helm.sh/}} chart + Docker\footnote{\url{https://www.docker.com/}} image + metadata.
I've created a library \verb|@avid-ps/gulp-clux| that provides a highly configurable task to build feature packs and you already installed it a few sections before.
Make sure you have Docker and Helm (v2.x.x)\footnote{\url{https://github.com/helm/helm/releases/tag/v2.16.10}} installed and available from the command-line.

\subsubsection{Create a feature pack build script}

Lets use the library to create a build script that builds a feature pack for the application.
Create the \verb|scripts/build-feature-pack.js| file and add the following code:

\begin{minted}{javascript}
  const path = require("upath");
  const gulp = require("gulp");
  const clux = require("@avid-ps/gulp-clux");
  const del = require("del");

  const projectDir = path.join(__dirname, "..");
  const dockerFile = path.join(projectDir, "docker", "Dockerfile");
  const appDir = path.join(projectDir, "src");
  const featurePackBuildDir = path.join(projectDir, "build", "featurePack");

  const appConfig = require(path.join(appDir, "package.json"));

  const clean = (files) => () => del(files);
  const build = clux.buildFeaturePack({ projectDir, appDir, dockerFile }, featurePackBuildDir);

  const featurePack = gulp.series(
  clean([featurePackBuildDir]),
  build,
  clean([
    `${featurePackBuildDir}/**/*`,
    `!${featurePackBuildDir}/install.sh`,
    `!${featurePackBuildDir}/${appConfig.identity.appName}-*.zip`,
    `!${featurePackBuildDir}`
  ]));

  featurePack(err => {
    if (err) {
      console.error(`Failed to build a feature pack. Error:`, err);
    } else {
      console.info(`Built a feature pack at ${featurePackBuildDir}`);
    }
  });
\end{minted}

First, I get paths to important project files and directories, then call the library function \verb|clux.buildFeaturePack| which creates a task that can build the feature pack.
Then I add that task to the build pipeline.
The pipeline performs the following steps:

\begin{enumerate}
\item Wipes out the feature pack build directory.
\item Builds feature pack.
\item Deletes all files from the feature pack directory except the feature pack itself and its installation script.
\end{enumerate}

\subsubsection{Create a Dockerfile}

Create the \verb|docker/Dockerfile| and add the following lines:

\begin{minted}{dockerfile}
  # Stage 1 - create cached node_modules
  # Only rebuild layer if package.json has changed
  FROM node:14.6.0 as cache
  WORKDIR /cache/
  COPY package.json .
  COPY .npmrc .
  RUN yarn install

  # Stage 2 - builder root
  FROM node:14.6.0 as build
  WORKDIR /root/
  # Copy node_modules
  COPY --from=cache /cache/ .
  # Copy source files, and possibly invalidate so we have to rebuild
  COPY . .
  RUN yarn run build:dev

  # Stage 3 - package
  FROM busybox
  COPY --from=build /root/build /dist/avid-mam-ps-clux-essence-info
\end{minted}

This file instructs docker to:

\begin{enumerate}
\item Build a separate layer with all project dependencies installed and cache it.
\item Use the cached layer to copy the dependencies and build the project using \verb|development| profile.
\end{enumerate}

Caching project dependencies speed ups the build dramatically.
The \verb|cloudux-starter-kit| provided by the product teams installs dependencies during each build which takes ages.
This is one of the reasons why I implemented the alternative solution.

\begin{exercise}[Analyse the Dockerfile]{exrcs:analyse-the-dockerfile}
  \begin{itemize}
  \item What happens on the each line of the Dockerfile?
  \item What are overlays?
  \item Is it possible to use variables inside the file? How?
  \item Can you pass variables to docker and use them inside the Dockerfile?
  \item How do you cleanup old images to free up drive space?
  \end{itemize}
\end{exercise}

Edit \verb|package.json| and add an alias to the \verb|scripts| object for the feature pack build script:

\begin{minted}{json}
  {
    "scripts": {
      "build:dev": "npx webpack --config webpack.dev.config.js --display-error-details",
      "build:prod": "npx webpack --config webpack.prod.config.js --display-error-details",
      "featurePack:dev": "node ./scripts/build-feature-pack.js"
    }
  }
\end{minted}

Now run the feature pack build:

\begin{minted}[linenos=false]{bash}
$ yarn run featurePack:dev
\end{minted}

Wait until it is finished and then review the content of the \verb|build/featurePack| directory:

\begin{minted}{bash}
$ ls build/featurePack
avid-mam-ps-clux-essence-info-0.0.1.zip
install.sh
\end{minted}

\begin{exercise}[Enhance the feature pack build script]{exrcs:enhance-fp-build-script}
  Update the script to accept an optional parameter which can be either \verb|dev| or \verb|prod| and defaults to \verb|dev| if is not provided in the command line.
  
  The script should use different docker images to build a development or production feature pack.
  Docker images will differ only in the command they use to build the project.
  Add \verb|featurePack:dev| and \verb|featurePack:prod| aliases to the \verb|package.json|.

  Find a better solution instead of using two almost identical docker files and explain how it works.
\end{exercise}

\subsection{Deploy the feature pack}

There are two options of installing the feature pack:
\begin{itemize}
\item Manual
\item Automatic
\end{itemize}

\subsubsection{Manual installation}

There is a good-enough™ documentation\footnote{\url{https://avid-ondemand.atlassian.net/wiki/spaces/PDT/pages/2182414693/Document+Viewer+How+to+Install}} that explains how to install the \textbf{Document Viewer} plug-in.
The process is indentical for the plug-in you just created and build.

\subsubsection{Automatic installation}
You can create a shell script to SSH upload the feature pack to the remote server, install it, and then delete installation files.

Create and edit the \verb|deploy.sh| script at the root of the project:

\begin{minted}{bash}
  #!/usr/bin/env sh

  # Command-line arguments
  # ---------------------------------------------------------------------
  CMD_ARG_SSH_SERVER=$1
  CMD_ARG_SSH_USER=$2

  # Config
  # ---------------------------------------------------------------------
  export LC_CTYPE=C

  if [ -n "${CMD_ARG_SSH_SERVER}" ]; then
  SSH_SERVER=$CMD_ARG_SSH_SERVER
  else
  SSH_SERVER=kl-psg-mcs89.global.avidww.com
  fi

  if [ -n "${CMD_ARG_SSH_USER}" ]; then
  SSH_USER=$CMD_ARG_SSH_USER
  else
  SSH_USER=root
  fi

  DEST_PACKAGES_DIR=/tmp
  SRC_DIR=build/featurePack

  SCP_CMD="scp"
  SSH_CMD="ssh"
  REMOTE_RUN_SCRIPT_CMD="sh"

  # Setup variables
  # ---------------------------------------------------------------------
  BLD=$(tput bold)
  NRM=$(tput sgr0)

  SSH_DESTINATION=$SSH_USER@$SSH_SERVER

  RANDOM_STR=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 32 | xargs)
  DEST_DIR=$DEST_PACKAGES_DIR/$RANDOM_STR

  # Deploy
  # ---------------------------------------------------------------------

  # 1. Copy package files to the remote host
  echo "Copying installation package files to ${BLD}${SSH_SERVER}:${DEST_DIR}${NRM}..."
  $SSH_CMD $SSH_DESTINATION mkdir -p "$DEST_DIR"
  $SCP_CMD $SRC_DIR/* $SSH_DESTINATION:"$DEST_DIR"

  # 2. Run installation script
  echo "Running ${BLD}${SSH_SERVER}:${DEST_DIR}/install.sh${NRM}..."
  echo "${BLD}----------------------------------------------------------------${NRM}"
  $SSH_CMD $SSH_DESTINATION "$REMOTE_RUN_SCRIPT_CMD" "$DEST_DIR/install.sh"
  echo "${BLD}----------------------------------------------------------------${NRM}"

  # 3. Cleanup
  echo "Deleting ${BLD}${SSH_SERVER}:${DEST_DIR}${NRM}..."
  $SSH_CMD $SSH_DESTINATION rm -rf "$DEST_DIR"
\end{minted}

Enable script execution:

\begin{minted}[linenos=false]{bash}
$ chmod +x ./deploy.sh
\end{minted}

The script accepts two optional arguments:

\begin{minted}[linenos=false]{bash}
$ ./deploy.sh server_name user_name
\end{minted}

If none is provided it defaults the first argument to \verb|kl-psg-mcs89.global.avidww.com| and second one to \verb|root|.

Run it to install the feature pack.
Depending on your SSH configuration it may ask password a few times.

\begin{exercise}[Use SSH keys instead of passwords]{exrcs:ssh-keys-auth}
  \begin{itemize}
  \item Read about SSH client configuration.
  \item Generate a local key and add public key to the server's known keys.
  \item Read how to unlock the key when you login to your local operating system.
  \item Configure the OS to unlock the key so that you do not need entering password each time the key is read by SSH.
  \end{itemize}

  Once it is done, you can run \verb|deploy.sh| without entering any passwords which makes its usage a breath.
\end{exercise}

\subsubsection{Install the feature pack}

Install the pack using \textbf{both} methods mentioned before for the sake of practice and make sure it appears in the Cloud UX.

Open the browser console and check the logs it writes when you use ``Open in > Essence Info'' context menu item.

\begin{exercise}[Plug-in icon]{exrcs:create-a-sexy-icon}
  Change the plug-in icon to something more sexier.
  Are there any colour and size restrictions?
\end{exercise}

\begin{exercise}[Make changes to the view]{exrcs:changes-to-the-view-1}
  Add a \verb|<span>| to the view.
  The span must display the name of the asset opened.
  Do not use React but plain DOM API.
\end{exercise}


\section{Accessing web-services from the plug-in}

Next time.
